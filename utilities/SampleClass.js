class SampleClass {

    constructor() {
        this.genericFunctions = require('../GenericFunctions.js');
    }
    
    sampleFunction() {
        return this.genericFunctions.formatError("501", "Function not implemented yet.");
    }

}

module.exports = new SampleClass();