const express = require('express');
const app = express();
const genericFunctions = require('./GenericFunctions.js');

const port = 8800;

app.all("/:class/:method", function(request, response) {
    const className = request.params.class;
    const methodName = request.params.method;
    try {
        const requestedClass = require('./utilities/'+className+'.js');
        try {
            response.json(requestedClass[methodName]());
        } catch (error) {
            response.json(genericFunctions.formatError("404", "Requested method not found."));
        }
    } catch (error) {
        response.json(genericFunctions.formatError("404", "Requested method not found."));
    }
});

app.all("*", function(request, response) { response.json(genericFunctions.formatError("404", "Requested method not found.")) });

app.listen(port, () => {
    console.log("Now listening on Port " + port);
});
